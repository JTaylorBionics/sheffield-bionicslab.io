---
title: "Arm: Linear Actuators"
date: 2020-08-18T11:20:07+01:00
author: Grace Faulkner
image: /images/blog/linear_actuator.png
description: "Arm Project Finalizes Linear Actuator Design For The Hand"
---

The arm project have recently been working on designing linear actuators in Fusion 360 which will be used to create movement in the joints of the bionic arm.

But what are linear actuators? Simply put, they are devices which create linear motion. This will allow the DC motors ordered and coded by the Firmware team to be placed away from the joint. This is necessary because if they were placed at the joint, they could take up too much space and may not produce enough torque.

{{<figure src="/images/blog/linear_actuator.png" title="Linear actuator design by Peter Mitrev" width="100%">}}

One linear actuator will be used for each finger with the actuator for the thumb and little finger located in the palm.

{{<figure src="/images/blog/palm_design.png" title="Palm design by Laurence Russell" width="100%">}}

The team has plans to develop a similar mechanism in the future for the elbow, and eventually the wrist, bringing Mark one step closer to a full range of movement in his prosthetic arm.
