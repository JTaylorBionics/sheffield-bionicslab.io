---
title: "Neil Rackham Foundation Award"
date: 2020-07-02T13:31:32+01:00
author: Thomas Binu Thomas
image: images/blog/neil_rackham/banner.jpg
description: "Our submission for the Neil Rackham Foundation award"
---

## An Introduction

As a way to fund our project this summer, the Arm Project competed in the Neil Rackham Award competition this year. The bursary was focused towards teams within the University of Sheffield that showed exemplary teamwork during the lockdown due to COVID-19. While we did not win the award we got the opportunity to reflect on all the work we during lockdown and got to learn from the teams that did win! Here's our submission.

## Our Submission

Sheffield Bionics Society is a student-led project at the University of Sheffield. We are determined to advance state-of-the-art in prosthetics in order to ameliorate the lives of people with disabilities and amputations.

The society comprises engineering students eager to apply the knowledge they learnt in the classroom in the context of prosthetics. We focus on three sections; arm, leg and brain computer interface, each led by a committee member, with the arm project being the most progressed.

{{<figure src="/images/blog/neil_rackham/arm1.png" title="A First Prototype"
           caption="One of the prototype arms created on Fusion 360 made by our lead structural design team member, Peter Mitrev" width="100%">}}

Every year we hold an Annual General Meeting (AGM), this year through Google Meet, to elect the next committee. During the AGM, we realised that we attracted many eager students. However, the majority left in the first few weeks as they lacked the skills necessary to work within such a challenging technical project. In response, we created a fourth subgroup that focused on member development. The group leader has been developing a completed and specific year-long course for beginners using weekly feedback from the other subgroup leaders, which will be published on our website, which we started in April. Although the results of this will be evident once the University reopens, we thought it would be beneficial to start this process early.

Despite the lockdown, the arm project team was determined to reach our goal of competing in CYBATHLON 2020 and found ways of collaborating remotely with members spread across the UK and abroad. The team leader liaised with the University, Academic Lead, CYBATHLON (all three via email) and the team. We used video conferences for weekly communication through Google Meet. While this was effective to inform every team member of the project progress, we later realised that splitting the calls into sub-teams allowed us to bring in more reserved members into discussions (easier in a home environment), thus strengthening the team bond and maximising project productivity. We found it very useful for the group leader to summarise the meeting, as attendance was not perfect, and the tasks at hand on our Messenger chat (voted medium) following each meeting and asking for feedback from the team members. One thing that we recently found that we plan on implementing is the use of Jamboard as a sticky-note alternative for brainstorming.

{{<figure src="/images/blog/neil_rackham/arm2.png" title="An Improved Elbow"
           caption="The design of an arm with a different elbow joint after having a crash course from a more senior society member done by Luc Curtis" width="100%">}}

Each subgroup needed to adjust the way that they worked. This was simplified by centralising files. Any administrative documents were stored on Google Drive, any mechanical designs were stored in a shared Fusion 360 folder and the electronic design and code was kept in GitHub. While this was tedious to set up at first, we found it very beneficial as every group member was able to access the work of any other member as reference. We found that there was some disparity with the knowledge of the team members. We quickly realised that if this was not addressed only a handful of members would be completing the project. The more advanced members spent the first couple of weeks providing workshops and useful links to the rest. This sharing of knowledge was essential to our progress.

“I delivered an online Fusion 360 crash course, highlighting the main tools that I used during my previous placement that I found worthwhile to learn when designing arm variations. I provided help to any member that had any issue while they were developing their own designs. I also was challenged to research better methods to model the hand that, like biomimetics that will be useful for the project.” - _Peter Mitrev, BEng Biomaterials_

The electronics subteam struggled the most due to the virus. Team members were scattered throughout the UK and Europe, yet had to work together for all to understand specific code which would be used to control an EMG sensor that moved the hand based on the pilot’s residual arm movement. This task was made particularity difficult by having to take turns to try and explain on screen with only a cursor as visual aid. Once achieved using screen-sharing and device control techniques, the team needed to test their code on the device, currently sat with a design team member who had little experience with coding. We guided him through installing the code using our newly-tried techniques and achieved amazing results! This was assisted greatly through external help and experience of a society alumni currently located in Cambridge.

{{<figure src="/images/blog/neil_rackham/pcb.png" title="The Hexadriver"
           caption="Design of the custom 'Hexadriver'. This is a custom PCB motor driver made specifically to be small yet powerful enough to move the prosthetic's fingers made by Will Bednall." width="100%">}}

“We learnt for this that there are benefits to remote working, as there are no geographical limitations to who we can call to ask for help, thus increasing the knowledge base of our team.” - _Nicholas Hagis, Bioengineering Student_

“I have been consulted by the Society to help design a cheap, compact linear actuator controller that I started while I was in the society during my final year at the University. It was adapted, during this period, to move the hands and replace the current bulky string-based design.” - _Will Bednall, University of Sheffield Alumni_

Our biggest achievement was finding a pilot for our hand after a two year search. He reminded us of why we are part of the society! Our talks with him shone a light on many overlooked factors making us go back and adapt our hand design for them (HALO-themed as per request).

“I found Mark Whitehead from his Facebook page “Homelessness in South Yorkshire”. Mark volunteers to help the homeless and those in need, regularly posting on the page. I noticed his prosthetic arm in one of the pictures and decided to contact him. Mark has been fantastic! He has quickly become an honorary team member and has given several talks about his personal experience.” - _Grace Faulkner, Health & Safety Officer_

“It has been a great, constant and iterative learning experience working during the pandemic, albeit challenging. I found being organised and transparent with my team and other stakeholders went a long way. A constant reminder of our reason, boosted through Mark, gave us our drive during quarantine. We are all proud and excited to continue working this summer in preparation for CYBATHLON in November, meeting up for online socials in between too!” - _Thomas Thomas, Arm Project Lead_
