---
title: "BBC Radio"
date: 2020-08-13T21:27:44+01:00
author: Grace Faulkner
image: images/blog/bbc-radio.png
description: "Listen to an interview of Sheffield Bionics on BBC Radio Sheffield"
---

Recently, Sheffield Bionics had a feature on BBC Radio Sheffield to discuss working with our pilot, Mark Whitehead, to build a bionic arm.

Mark and Arm Project Leader, Thomas Binu Thomas, were both interviewed on the show on 23rd July 2020 to talk about what the project means to them.

This will be the first time the arm project is working with a pilot and the project has developed phenomenally despite initial complications due to the lockdown, including redesigning the arm to be HALO-themed for Mark.

You can listen to the interview here:

{{< audio "/audio/radio_sheffield.mp3" >}}
