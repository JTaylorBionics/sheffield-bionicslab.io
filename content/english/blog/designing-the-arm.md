---
title: "Designing the Arm"
date: 2020-09-01T17:55:44+01:00
author: Grace Faulkner
image: images/blog/designing-the-arm/thumb.jpg
description: "The structural team have been working to develop a HALO themed arm for Mark"
---

The arm team have been working tirelessly these last few weeks. From modelling the electrical components to designing key structural elements, the design for the arm is starting to take shape.

The inspiration for the look of the arm will be HALO themed, at Mark's request. Thomas Thomas, leader of the arm project said, "We want to make it an arm that he is very proud to wear."

{{<figure src="/images/blog/designing-the-arm/halo.png" caption="The image the team used for inspiration when creating a HALO themed arm." width="40%">}}

Along with the aesthetics of the arm, the team have been focusing on creating a biomimetic hand for the prosthetic. To do this, they used open-source CT scans of fingers, and then used mesh editing to reduce noise and create a more accurate representation. This design is currently being 3D printed. This will be the third hand the arm team have produced over the year, starting with two open source designs from OpenBionics.

{{<figure src="/images/blog/designing-the-arm/forearm.jpg" caption="All the structural elements are modelled in Fusion 360, a CAD (computer aided design) software." width="70%">}}

Finally, the team have also been working on a design for the elbow. When all these have been perfected, the first iteration of Mark's arm will have been designed.