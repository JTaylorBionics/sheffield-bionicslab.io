---
title: "Leg Electronics Team - Progress!"
date: 2021-02-26T14:12:55Z
author: Grace Faulkner, Bhoomika Gandhi and Mika Takigawa
image: images/blog/US_system_setup.jpg
description: "The leg electronics team have been working on a system using ultrasonic sensors."
---



Mika, Bhoomika and Grace have been working on a project last Semester using ultrasonic sensors to determine angle of foot to ground.

### Background

After speaking with an amputee, it was realised that the current mechanical angles are not ideal for use on an inclined ground. It causes some discomfort in walking for the user. So, the electronics team for the leg project decided to work on a system to determine the angle of the foot from the ground. This was done to add an automatic powered ankle that could move on its own by sensing the inclination of the ground. So, the first step was to sense the inclination.

To determine this, the team decided to use some sensors to determine the distance from the ground. Originally, IR (infrared sensors) were considered however it was found that IR sensors are less accurate in ambient light. Instead, ultrasonic sensors were chosen to be placed one at the toe and one at the heel of the foot. Then, the angle of the foot to the ground could be determined using trigonometry. Ideally, we want to keep the angle minimum (i.e. keep the foot parallel to the ground) for ease of walking.

{{<figure src="/images/blog/US_sensors.png" caption="Ultrasonic sensors used" width="40%">}}

  ### Modelling

To build this system, the team first created a simulation using a TinkerCAD model. This is an Autodesk program which allows the user to simulate simple electronics projects, including the code in an Arduino IDE environment. This program allowed collaboration between the team, despite not being able to meet in person. There were some drawbacks to this system, however, as there was minor desync between the users, which originally caused some confusion and redundant code. From then on, the team nominated an editor for the meeting but all members were able to run the simulation.

{{<figure src="/images/blog/US_sim.png" caption="TinkerCAD model of the system." width="40%">}}


During this time, the electronics team worked with the structure team to create a simple clip to hook the ultrasonic detector to the shoe for testing. This clip was then 3D printed and used as the code was tested later.


{{<figure src="/images/blog/3d_printed_clip.png" caption="3D printed clip produced by structure team to clip the sensor to a shoe" width="40%">}}

  ### Testing

Once the code was working in the simulation, Mika was able to pick up the components to replicate the system in real life. In order to test the system worked, the sensors were attached to a shoe and placed on level ground. The result from the program was then checked to see if the angle given was 0 degrees.

{{<figure src="/images/blog/US_system_setup.jpg" caption="The final set-up attached to a shoe to test." width="40%">}}

  ### Calibration and Next Steps

The code was then edited to allow calibration. However, this is currently implemented at the start of the code and so the code must be rerun every time the system is checked. Moving forward, the team plans to add a button to press when calibration is required. To ensure the automated power ankle works, the team is currently working with the structural and the firmware team to connect a motor responsible for movement and controlling this movement. The team also aims to design a custom PCB for the ultrasonic sensors to make it compact. At the moment, the sensors add bulk to a shoe which may be limiting to the user in terms of the function and aesthetics of their shoe.