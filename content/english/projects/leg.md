---
title: "Leg Project"
date: 2020-07-04T20:57:18+01:00
author: Grace Faulkner
image: images/blog/leg_banner.jpg
description: "A miniature prototype of a bionic leg"
---

The leg project is aiming to produce a bionic leg for 2022. We will be starting in September to work on the ideas phase of the design.

## The Plan

Last year, the leg project focused on developing the skills of its members. However, with the creation of a fourth project aimed at developing new members, this year the leg project will be able to focus more on our goals.

We are split into two main teams: structure and electronics. The structural team is looking to develop the frame of the leg and works with CAD software to design the shape and structure, while the electronics team is working to develop the leg's control systems.

The leg project is aiming to produce the project in time for the bi-annual mini-Cybathlon in 2022 so the next couple of semesters are crutial for ensuring we are set up to meet this goal.

## Who We're Looking For

We are looking for hard working, motivated students who would be able to commit a minimum of 2 hours a week. Because this year we will be more or less starting from scratch, anyone who is interested in design and who is creative will be particularly useful.

A working knowledge of any of the following would be great but is not essential:

- Programming
- CAD software (Fusion 360)
- Electronics
- 3D printing

## Sponsorship

Although we are currently generating ideas, we will be looking soon to be able to test these ideas. Funding will allow us to buy the materials we will need to do this, and so ensure the quality of our final design.
